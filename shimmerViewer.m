function shimmerViewer(filename)
%debugging\[
filename = 'C:\Projects\IMUHumanPose\data\syncWalking\syncWalking.dat';
%\]
% extract information like number of shimmers, length of recording, types
% of available data, column-indices for each shimmer and datatype
params = getRecordingParams(filename);

% create body model struct with origin, limb lengths, initial rotation,
% shoulder and hip width etc., Number of tracked limbs depends on the
% number of shimmers present in the recording file
bodyModel = createBodyModel(params);

playShimmerRecordingRAM(params, bodyModel);

end

function params = getRecordingParams(filename)
%debugging\[
% filename = 'D:\Projects\IMUHumanPoseEstimation\data\syncTest_50Hz_60secInterval.dat';
%\]
shiF = fopen(filename);

firstRow = fgetl(shiF);
shimmerNames = regexp(firstRow,'\t','split');

secondRow = fgetl(shiF);
dataTypes = regexp(secondRow,'\t','split');

thirdRow = fgetl(shiF);
calOrRaw = regexp(thirdRow,'\t','split');

fourthRow = fgetl(shiF);
unitNames = regexp(fourthRow,'\t','split');

dataStartPosition = ftell(shiF);
% count number of lines
numSamples = 0;
% while 1
%     % scanning a file with 10 sensors and 5 datatypes takes about
%     % 1sec/minute
%     tmp = fgetl(shiF);
%     if ~ischar(tmp), break, end
%     numSamples = numSamples + 1;
% end

% determine data types in the order of the .dat file
idxFirstSensor = regexp(firstRow, [shimmerNames{1} '\t'], 'start');
for i=1:numel(idxFirstSensor)
    params.dataTypes{i} = dataTypes{i};
end

params.numShimmers = numel(unique(shimmerNames));

sensorNames = unique(shimmerNames, 'stable');
for i=1:params.numShimmers
    splitName = regexp(sensorNames{i},'Shimmer','split');
    params.sensorIDs(i) = str2double(splitName{2});
end

params.numSamples = numSamples;
params.dataStart = dataStartPosition;
params.filename = filename;

fclose(shiF);
% available fields
% params.numSamples = numSamples;
% params.dataStart = dataStartPosition;
% params.filename = filename;
% params.numShimmers = numel(unique(shimmerNames));
% params.sensorIDs(i) = str2double(splitName{2});
% params.dataTypes{i} = dataTypes{i};
end

function bodyModel = createBodyModel(params)

bodyModel.center = [0 0 0]';
bodyModel.params.shoulderWidth = 200;
bodyModel.params.hipWidth = 150;
bodyModel.params.upperAbdomenLength = 600;
bodyModel.params.lowerAbdomenLength = 50;

initialLeftShoulderDirection = [0 -bodyModel.params.upperAbdomenLength bodyModel.params.shoulderWidth]';
initialLeftShoulderDirection = initialLeftShoulderDirection/norm(initialLeftShoulderDirection);
initialRightShoulderDirection = [0 -bodyModel.params.upperAbdomenLength -bodyModel.params.shoulderWidth]';
initialRightShoulderDirection = initialRightShoulderDirection/norm(initialRightShoulderDirection);

initialLeftHipDirection = [-bodyModel.params.hipWidth 0 -bodyModel.params.lowerAbdomenLength]';
initialLeftHipDirection = initialLeftHipDirection/norm(initialLeftHipDirection);
initialRightHipDirection = [bodyModel.params.hipWidth 0 -bodyModel.params.lowerAbdomenLength]';
initialRightHipDirection = initialRightHipDirection/norm(initialRightHipDirection);

shoulderLimbLength = sqrt(bodyModel.params.upperAbdomenLength^2 + bodyModel.params.shoulderWidth^2);
hipLimbLength = sqrt(bodyModel.params.lowerAbdomenLength^2 + bodyModel.params.hipWidth^2);

% arms (leftLower, leftUpper, rightLower, rightUpper)
bodyModel.limbs{1} = makeLimbStruct(250,[0 0 -1]');
bodyModel.limbs{2} = makeLimbStruct(250,[0 1 0]');
bodyModel.limbs{3} = makeLimbStruct(250,[0 -1 0]');
bodyModel.limbs{4} = makeLimbStruct(250,[0 0 1]');
% legs (leftLower, leftUpper, rightLower, rightUpper)
bodyModel.limbs{5} = makeLimbStruct(400,[0 0 -1]');
bodyModel.limbs{6} = makeLimbStruct(400,[0 0 -1]');
bodyModel.limbs{7} = makeLimbStruct(400,[0 -1 0]');
bodyModel.limbs{8} = makeLimbStruct(400,[0 -1 0]');
% head
bodyModel.limbs{9} = makeLimbStruct(220);
% upper abdomen
bodyModel.limbs{10} = makeLimbStruct(600);
% lower abdomen
bodyModel.limbs{11} = makeLimbStruct(0,[0 0 -1]');
% center to shoulders (left, right)
bodyModel.limbs{12} = makeLimbStruct(shoulderLimbLength,initialLeftShoulderDirection);
bodyModel.limbs{13} = makeLimbStruct(shoulderLimbLength,initialRightShoulderDirection);
% center to femur bones (left, right)
bodyModel.limbs{14} = makeLimbStruct(hipLimbLength,initialLeftHipDirection);
bodyModel.limbs{15} = makeLimbStruct(hipLimbLength,initialRightHipDirection);

switch params.numShimmers
    case 1
    case 9
    case 10
        % child/sibling connections between limbs
        bodyModel.limbs{2}.children = 1;
        bodyModel.limbs{4}.children = 3;
        bodyModel.limbs{6}.children = 5;
        bodyModel.limbs{8}.children = 7;
        bodyModel.limbs{10}.children = 9;
        bodyModel.limbs{12}.children = 2;
        bodyModel.limbs{13}.children = 4;
        bodyModel.limbs{14}.children = 6;
        bodyModel.limbs{15}.children = 8;
        bodyModel.limbs{10}.siblings = {12, 13};
        bodyModel.limbs{11}.siblings = {14, 15};
        % these limbs have no parents(!). They are attached to the center.
        bodyModel.rootIdx = {10, 11};
    case 11

    otherwise
        error(['Body model could not be generated with the number of Shimmers detected (' num2str(params.numShimmers) ' Shimmers).'])
end

function limb = makeLimbStruct(lengthmm, initDir)
limb.length = lengthmm;
limb.initialDirection = [0 -1 0]';
limb.parent = [];
limb.child = [];
limb.sibling = [];
if nargin > 1
    limb.initialDirection = initDir;
end
end

end

function playShimmerRecording(params, bodyModel)
shiF = fopen(params.filename);    
fseek(shiF,params.dataStart,-1);

shimmerQuaternions = cell(1,params.numShimmers);

quaternionColumns = {};
% find quaternion columns
for typeInd = 1:numel(params.dataTypes)
    if ~isempty(regexp(params.dataTypes{typeInd},'Quat','match'))
        quaternionColumns{end+1} = typeInd;
    end
end
quaternionColumns = cell2mat(quaternionColumns);
quaternionColumns = repmat(quaternionColumns,1,params.numShimmers) + numel(params.dataTypes)*(ceil((1:(params.numShimmers*4))/4)-1);
numSample = 0;
tic
while 1
    tmp = fgetl(shiF);
    if ~ischar(tmp), break, end
    numSample = numSample+1;
    values = regexp(tmp,'\t','split');
    tempquat = str2double(values(quaternionColumns));
    for shimmerInd = 1:params.numShimmers
        % TODO: profiler tells that str2double is expensive: preprocess
        % quaternionColumns so that it indices the whole row
%         tempquat = str2double(values(quaternionColumns+numel(params.dataTypes)*(shimmerInd-1)));
        % assign quaternion to correct index position (=shimmer number)
        shimmerQuaternions{params.sensorIDs(shimmerInd)} = tempquat(((shimmerInd-1)*4)+(1:4))';
    end
    h = showBodyModel(params, bodyModel, shimmerQuaternions);
    time = toc;
    tic
    set(h,'string',['sample ' num2str(numSample) ', readout frequency: ' num2str(round(1/time)) 'Hz.'])
    drawnow
%     pause(0.01)
end
end

function playShimmerRecordingRAM(params, bodyModel)
% available fields:
% params.numSamples = numSamples;
% params.dataStart = dataStartPosition;
% params.filename = filename;
% params.numShimmers = numel(unique(shimmerNames));
% params.sensorIDs(i) = str2double(splitName{2});
% params.dataTypes{i} = dataTypes{i};

[folder,dataBaseName]=fileparts(params.filename);
matFiles = dir([folder filesep '*.mat']);

if isempty(matFiles)

    shiF = fopen(params.filename);
    formatString = [];
    skipFormatString = [];
    for i=1:numel(params.dataTypes)
        firstPart = regexp(params.dataTypes{i},' ','split');
        switch firstPart{1}
            case 'Timestamp'
                formatString = [formatString '%s'];
                skipFormatString = [skipFormatString '%*s'];
            otherwise
                formatString = [formatString '%f'];
                skipFormatString = [skipFormatString '%*f'];
        end     
    end
    formatStringNew = [];
    skipShimmerNumbers = input('Which Shimmers were not synchronized? Multiple numbers as vector [i j ...], press "Enter" to skip: ');
    for i=1:numel(skipShimmerNumbers)
        skipShimmerNumbers(i)= find(params.sensorIDs==skipShimmerNumbers(i));
    end
    validIDs = true(size(params.sensorIDs));
    validIDs(skipShimmerNumbers) = false;
    params.sensorIDs = params.sensorIDs(validIDs);

    for i=1:params.numShimmers
        if ~any(i==skipShimmerNumbers)
            formatStringNew = [formatStringNew formatString];
        else
            formatStringNew = [formatStringNew skipFormatString];
        end
    end
    formatString = formatStringNew;

    fseek(shiF,params.dataStart,-1);
    shimDat = textscan(shiF, formatString, 'delimiter', '\t');   
    fclose(shiF);

    % convert all timestamps to numeric values
    endTimes = zeros(1,params.numShimmers-numel(skipShimmerNumbers));
    shimInd = 1;
    for col=1:numel(params.dataTypes):numel(shimDat)
        for i=1:numel(shimDat{1})
            try
                % Use this for the calibrated timestamps: datenum('08/02/2015 17:41:18.006','dd/mm/yyyy HH:MM:SS.FFF')
                shimDat{col}{i} = datenum(shimDat{col}{i},'dd/mm/yyyy HH:MM:SS.FFF');
            catch
                endTimes(shimInd)=shimDat{col}{i-1};
                shimDat{col}(i:end) = num2cell(NaN(numel(shimDat{1})-i+1,1));
                break;
            end
        end
        shimDat{col} = cell2mat(shimDat{col});
        disp(['Shimmer ' num2str(shimInd) ' processed (out of ' num2str(numel(endTimes)) ').'])
        shimInd = shimInd+1;
    end

    shimDat = cell2mat(shimDat);
% size after this conversion (all doubles because of the timestamps): 58kB
% per second (!), this makes 208MB/hour. Still fits in RAM.
    save([folder filesep dataBaseName '.mat'],'shimDat','skipShimmerNumbers','params','endTimes');
else
    load([folder filesep matFiles(1).name]);
end
% find quaternion columns
quaternionColumns = {};
for typeInd = 1:numel(params.dataTypes)
    if ~isempty(regexp(params.dataTypes{typeInd},'Quat','match'))
        quaternionColumns{end+1} = typeInd;
    end
end
quaternionColumns = cell2mat(quaternionColumns);
quaternionColumns = repmat(quaternionColumns,1,params.numShimmers-numel(skipShimmerNumbers)) + numel(params.dataTypes)*(ceil((1:((params.numShimmers-numel(skipShimmerNumbers))*4))/4)-1);
quaternionColumns = mat2cell(quaternionColumns,1,4*ones(1,params.numShimmers-numel(skipShimmerNumbers)));
% sort the whole recording by its timestamps!
timeList = reshape(shimDat(:,1:numel(params.dataTypes):end),[],1);
numRec = size(shimDat,1);
runningIndices = uint32(repmat((1:numRec)',params.numShimmers-numel(skipShimmerNumbers),1));
runningIndices = runningIndices(~isnan(timeList));
shimmerIndices = uint8(ceil((1:numel(timeList))'/numRec));
shimmerIndices = shimmerIndices(~isnan(timeList));
timeList = timeList(~isnan(timeList));
[sortedTimeList,sortedIndices]=sort(timeList);
runningIndices = runningIndices(sortedIndices);
shimmerIndices = shimmerIndices(sortedIndices);

% find out the earliest synchronized timestamp and the latest one
startTimes = shimDat(1,1:numel(params.dataTypes):end);
latestStartTime = max(startTimes);
earliestEndTime = min(endTimes(endTimes~=0));
startIndex = find(sortedTimeList==latestStartTime,1,'first');
endIndex = find(sortedTimeList==earliestEndTime,1,'last');
startTime = sortedTimeList(startIndex);
tic
% numSample = 1;
% NSamples = sum(~isnan(timeList));
numSample = startIndex;
NSamples = endIndex-startIndex+1;
shimmerQuaternionsOld = repmat([1 0 0 0]',1,params.numShimmers);
while numSample < NSamples
    shimmerIndVec = [];
    shimmerQuaternionsTmp = [];
    while ~any(shimmerIndices(numSample)==shimmerIndVec)
        shimmerIndVec = [shimmerIndVec shimmerIndices(numSample)];
        shimmerQuaternionsTmp = [shimmerQuaternionsTmp shimDat(runningIndices(numSample),quaternionColumns{shimmerIndices(numSample)})];
        if numSample < NSamples
            numSample = numSample+1;
        else
            break;
        end
    end
    shimmerQuaternionsTmp = reshape(shimmerQuaternionsTmp,4,[]);
    shimmerQuaternions = shimmerQuaternionsOld;
    shimmerQuaternions(:,params.sensorIDs(shimmerIndVec)) = shimmerQuaternionsTmp;
    shimmerQuaternionsOld = shimmerQuaternions;
    shimmerQuaternions = mat2cell(shimmerQuaternions,4,ones(1,params.numShimmers));
    
    h = showBodyModel(params, bodyModel, shimmerQuaternions);
    time = toc;
    tic
    set(h,'string',['Time: ' datestr(sortedTimeList(numSample)-startTime,13) ', readout frequency: ' num2str(round(1/time)) 'Hz.'])
    drawnow
%     pause(0.01)
end

end

function cpyText = showBodyModel(params, bodyModel, shimmerQuaternions)
persistent Hfigure HbodyParts Htext
if isempty(Hfigure) || isempty(HbodyParts)
    Hfigure = figure;
    lx = [0 1];
    ly = [0 0];
    lz = [0 0];
    colors = lines(numel(bodyModel.limbs));
    for i=1:numel(bodyModel.limbs)
        hold on
        HbodyParts(i) = line('XData',lx, 'YData',ly, 'ZData',lz,'Color',colors(i,:),'LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
        hold off
    end
    axis vis3d image %off
    axis([[-1000 1000] [-1500 1500] [-1000 1000]])
    view(3)
    Htext = text(100,100,1000,'sample 0');
end
cpyText = Htext;
heap = bodyModel.rootIdx;
while ~isempty(heap)
cchildren={};
csiblings={};
for rr=1:numel(heap)
    rInd = heap{rr};
    updateLimb(bodyModel.limbs{rInd},rInd);
end
heap = [cchildren(:)' csiblings(:)'];
end

function updateLimb(limb,idx)
    if idx <= params.numShimmers
    limb.quat = shimmerQuaternions{idx};
    newDirection = qRotatePoint(limb.initialDirection,limb.quat);
    elseif isfield(limb,'quat')
    newDirection = qRotatePoint(limb.initialDirection,limb.quat);
    else
    newDirection = limb.initialDirection;
    limb.quat = [1 0 0 0]';
    end
    limb.endpoint = newDirection * limb.length;
    if isfield(limb,'startpoint')
    set(HbodyParts(idx),'XData',[limb.startpoint(1) limb.startpoint(1)+limb.endpoint(1)],'YData',[limb.startpoint(2) limb.startpoint(2)+limb.endpoint(2)],'ZData',[limb.startpoint(3) limb.startpoint(3)+limb.endpoint(3)]);
    limb.endpoint = limb.endpoint+limb.startpoint;
    else
    set(HbodyParts(idx),'XData',[bodyModel.center(1) limb.endpoint(1)],'YData',[bodyModel.center(2) limb.endpoint(2)],'ZData',[bodyModel.center(3) limb.endpoint(3)]);
    end
    if isfield(limb,'children')
        cchildren{end+1} = limb.children;
        bodyModel.limbs{limb.children}.startpoint = limb.endpoint;
    end
    if isfield(limb,'siblings')
        csiblings = [csiblings(:)' limb.siblings(:)'];
        for s=1:numel(limb.siblings)
            bodyModel.limbs{limb.siblings{s}}.quat = limb.quat;
        end
    end
end

end
