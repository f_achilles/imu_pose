function [hypot1,hypot2,hypot3,hypot4,body1,Head] = CreateBody(origin,rot_z,body,head)
    body1_angle       = deg2rad(90);   
    rotQ1             = qGetRotQuaternion(body1_angle,rot_z); 
    par_rotpoint1     = qRotatePoint(body.point_rot,rotQ1); 
    par1_rotpt_len    = par_rotpoint1 * body.body1_length;                      
    body1             = par1_rotpt_len + origin; 
    shoulder          = [200;0;0];
    hypot1            = sqrt((body1.^2) + (shoulder.^2));
    hypot2            = [-hypot1(1);hypot1(2);hypot1(3)];
    
    %%%HEAD
%     Rot_Matrix_x      = [0 0 1;0 cos(head.angle_x) -sin(head.angle_x) ; 0 sin(head.angle_x) cos(head.angle_x)];
%     Rot_Matrix_y      = [cos(head.angle_y) 0 -sin(head.angle_y) ;0 1 0; sin(head.angle_y) 0 cos(head.angle_y)];
%     Rot_Matrix_z      = [cos(head.angle_z) -sin(head.angle_z) 0 ; sin(head.angle_z) cos(head.angle_z) 0 ; 0 0 1];
%     Rot_xyzaxes       = Rot_Matrix_z * Rot_Matrix_x * Rot_Matrix_y;     %%rotation around z axis
%     rotQ2             = qGetQ( Rot_xyzaxes ); %child quaternion
    rotQ2            = qGetRotQuaternion(head.angle,rot_z); 
    head_rotpoint     = qRotatePoint(body.point_rot,rotQ2); 
    par_rotpt_len     = head_rotpoint * head.length;                      
    Head              = par_rotpt_len + body1;

    body2_angle       = deg2rad(-90);   
    rotQ3             = qGetRotQuaternion(body2_angle,rot_z); 
    par_rotpoint2     = qRotatePoint(body.point_rot,rotQ3); 
    par2_rotpt_len    = par_rotpoint2 * body.body2_length;                      
    body2             = par2_rotpt_len + origin;
    hip               = [100;0;0];
    hypot3            = -sqrt((body2.^2) + (hip.^2));
    hypot4            = [-hypot3(1);hypot3(2);hypot3(3)];

end