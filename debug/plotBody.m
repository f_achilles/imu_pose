function plotBody(origin,UpperBody,body,Head)
x               = [UpperBody(1),Head(1)];
y               = [UpperBody(2),Head(2)];
z               = [UpperBody(3),Head(3)];
xx              = [origin(1),UpperBody(1)];
yy              = [origin(2),UpperBody(2)];
zz              = [origin(3),UpperBody(3)];
xx1             = [origin(1),body.initpos_RightArm(1)];
yy1             = [origin(2),body.initpos_RightArm(2)];
zz1             = [origin(3),body.initpos_RightArm(3)];
xx2             = [origin(1),body.initpos_LeftArm(1)];
yy2             = [origin(2),body.initpos_LeftArm(2)];
zz2             = [origin(3),body.initpos_LeftArm(3)];
xx3             = [origin(1),body.initpos_RightLeg(1)];
yy3             = [origin(2),body.initpos_RightLeg(2)];
zz3             = [origin(3),body.initpos_RightLeg(3)];
xx4             = [origin(1),body.initpos_LeftLeg(1)];
yy4             = [origin(2),body.initpos_LeftLeg(2)];
zz4             = [origin(3),body.initpos_LeftLeg(3)];
axis([-1000 1500,-1000 1000,-1000 1000]);
hold on
body_main = line('XData',xx, 'YData',yy, 'ZData',zz,'Color','r','LineStyle','--','Marker','*','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
body_head = line('XData',x, 'YData',y, 'ZData',z,'Color','k','LineStyle','--','Marker','o', 'MarkerSize',6, 'LineWidth',2);
body_ur   = line('XData',xx1, 'YData',yy1, 'ZData',zz1,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
body_ul   = line('XData',xx2, 'YData',yy2, 'ZData',zz2,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);

body_lr   = line('XData',xx3, 'YData',yy3, 'ZData',zz3,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
body_ll   = line('XData',xx4, 'YData',yy4, 'ZData',zz4,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
 
hold off