function [parent,child,rotQ,chi_Q] = CreateLimbs3D(point_rot,hypot,limbs)

%parent
%rotQ            = qGetRotQuaternion(par_angle,rot_z); %Rotation Quaternion
Rot_Matrix_x     = [0 0 1;0 cos(limbs.parangle.x) -sin(limbs.parangle.x) ; 0 sin(limbs.parangle.x) cos(limbs.parangle.x)];
Rot_Matrix_y     = [cos(limbs.parangle.y) 0 -sin(limbs.parangle.y) ;0 1 0; sin(limbs.parangle.y) 0 cos(limbs.parangle.y)];
Rot_Matrix_z     = [cos(limbs.parangle.z) -sin(limbs.parangle.z) 0 ; sin(limbs.parangle.z) cos(limbs.parangle.z) 0 ; 0 0 1];
Rot_xyzaxes      = Rot_Matrix_z * Rot_Matrix_x * Rot_Matrix_y;     %%rotation around z axis
rotQ            = qGetQ( Rot_xyzaxes ); %child quaternion
par_rotpoint    = qRotatePoint(point_rot,rotQ); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * limbs.parlength; 
parent          = par_rotpt_len + hypot;
%end point of the parent

%child
    
% time             = 5;
% frequency        = 0.1;
% theta1           = (sin(2 * pi * frequency * time)) + chi_angle ; %sine motion

%chi_Q            = qGetRotQuaternion(chi_angle,rot_z);
Rot_Matrix_x     = [0 0 1;0 cos(limbs.chiangle.x) -sin(limbs.chiangle.x) ; 0 sin(limbs.chiangle.x) cos(limbs.chiangle.x)];
Rot_Matrix_y     = [cos(limbs.chiangle.y) 0 -sin(limbs.chiangle.y) ;0 1 0; sin(limbs.chiangle.y) 0 cos(limbs.chiangle.y)];
Rot_Matrix_z     = [cos(limbs.chiangle.z) -sin(limbs.chiangle.z) 0 ; sin(limbs.chiangle.z) cos(limbs.chiangle.z) 0 ; 0 0 1];
Rot_xyzaxes      = Rot_Matrix_z * Rot_Matrix_x * Rot_Matrix_y;     %%rotation around z axis
chi_Q             = qGetQ( Rot_xyzaxes ); %child quaternion
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * limbs.chilength;
child            = chi_rotpt_len + parent;
disp(parent);
disp(child);

end