function plotLimbs(parent,child,hypot)
lx       = [hypot(1),parent(1)];
ly       = [hypot(2),parent(2)];
lz       = [hypot(3),parent(3)];
lx1      = [parent(1),child(1)];
ly1      = [parent(2),child(2)];
lz1      = [parent(3),child(3)];
par_Limb = line('XData',lx, 'YData',ly, 'ZData',lz,'Color','b','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
chi_Limb = line('XData',lx1, 'YData',ly1, 'ZData',lz1,'Color','g','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
 hold on;
end