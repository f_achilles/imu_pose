close all
parameters();

%% Body
[body.initpos_RightArm,body.initpos_LeftArm,body.initpos_RightLeg,body.initpos_LeftLeg,UpperBody,Head] = CreateBody3D(origin,rot_z,body,head);
plotBody(origin,UpperBody,body,Head)

%% Upper right limb
[RightArm.parent,RightArm.child,RightArm.par_Q,RightArm.chi_Q] = CreateLimbs3D(body.point_rot,body.initpos_RightArm,RightArm);
plotLimbs(RightArm.parent,RightArm.child,body.initpos_RightArm);

%% Upper left limb
[LeftArm.parent,LeftArm.child,LeftArm.par_Q,LeftArm.chi_Q]     = CreateLimbs3D(body.point_rot,body.initpos_LeftArm,LeftArm);
plotLimbs(LeftArm.parent,LeftArm.child,body.initpos_LeftArm);

%% Lower Right Limbs
[RightLeg.parent,RightLeg.child,RightLeg.par_Q,RightLeg.chi_Q] = CreateLimbs3D(body.point_rot,body.initpos_RightLeg,RightLeg);
plotLimbs(RightLeg.parent,RightLeg.child,body.initpos_RightLeg);

%% Lower left Limbs
[LeftLeg.parent,LeftLeg.child,LeftLeg.par_Q,LeftLeg.chi_Q]     = CreateLimbs3D(body.point_rot,body.initpos_LeftLeg,LeftLeg);
plotLimbs(LeftLeg.parent,LeftLeg.child,body.initpos_LeftLeg);

%% 
xlabel('X')
zlabel('Z')
ylabel('Y')

axis vis3d 