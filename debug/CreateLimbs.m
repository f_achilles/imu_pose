function [parent,child,rotQ,chi_Q] = CreateLimbs(rot_z,point_rot,hypot,arm)

%parent
rotQ            = qGetRotQuaternion(arm.parangle,rot_z); %Rotation Quaternion
par_rotpoint    = qRotatePoint(point_rot,rotQ); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * arm.parlength; 
parent          = par_rotpt_len + hypot;
%end point of the parent

%child
    
% time             = 5;
% frequency        = 0.1;
% theta1           = (sin(2 * pi * frequency * time)) + chi_angle ; %sine motion

chi_Q            = qGetRotQuaternion(arm.chiangle,rot_z);
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * arm.chilength;
child            = chi_rotpt_len + parent;
disp(parent);
disp(child);

end