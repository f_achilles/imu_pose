close all

%%Body
body.body1_length     = 600;%middle line
body.head_length      = 220;
body.body2_length     = 50;%for the bottom 
origin                = [0;0;0];
rot_z                 = [0,0,1];
body.point_rot        = [1;0;0]; 

%% Body
[body.initpos_RightArm,body.initpos_LeftArm,body.initpos_RightLeg,body.initpos_LeftLeg,UpperBody,Head] = CreateBody(origin,rot_z,body);
plotBody(origin,UpperBody,body,Head)

%% Upper right limb
RightArm.parlength = 250;
RightArm.chilength = 250;
RightArm.parangle  = deg2rad(60);%parent angle:-can be a negative angle or a positive angle
RightArm.chiangle  = deg2rad(60);
[RightArm.parent,RightArm.child,RightArm.par_Q,RightArm.chi_Q] = CreateLimbs(rot_z,body.point_rot,body.initpos_RightArm,RightArm);
plotLimbs(RightArm.parent,RightArm.child,body.initpos_RightArm);


%% Upper left limb
LeftArm.parlength = 250;
LeftArm.chilength = 250;
LeftArm.parangle  = deg2rad(120);%parent angle:-can be a negative angle or a positive angle
LeftArm.chiangle  = deg2rad(120);
[LeftArm.parent,LeftArm.child,LeftArm.par_Q,LeftArm.chi_Q] = CreateLimbs(rot_z,body.point_rot,body.initpos_LeftArm,LeftArm);
plotLimbs(LeftArm.parent,LeftArm.child,body.initpos_LeftArm);

%% Lower Right Limbs
RightLeg.parlength = 400;
RightLeg.chilength = 400;
RightLeg.parangle  = deg2rad(-120);
RightLeg.chiangle  = deg2rad(-120);
[RightLeg.parent,RightLeg.child,RightLeg.par_Q,RightLeg.chi_Q] = CreateLimbs(rot_z,body.point_rot,body.initpos_RightLeg,RightLeg);
plotLimbs(RightLeg.parent,RightLeg.child,body.initpos_RightLeg);
%% Lower left Limbs
LeftLeg.parlength = 400;
LeftLeg.chilength = 400;
LeftLeg.parangle  = deg2rad(-60);
LeftLeg.chiangle  = deg2rad(-60);
[LeftLeg.parent,LeftLeg.child,LeftLeg.par_Q,LeftLeg.chi_Q] = CreateLimbs(rot_z,body.point_rot,body.initpos_LeftLeg,LeftLeg);
plotLimbs(LeftLeg.parent,LeftLeg.child,body.initpos_LeftLeg);

xlabel('X')
zlabel('Z')
ylabel('Y')

axis vis3d 