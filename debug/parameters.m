
body.body1_length     = 600;%middle line
body.body2_length     = 50;%for the bottom 
body.origin           = [0;0;0];
body.rot_z            = [0,0,1];
body.point_rot        = [1;0;0];

origin = body.origin;
rot_z = body.rot_z;

%% Head
head.length           = 220;
head.angle.x          = deg2rad(90);
head.angle.y          = deg2rad(90);
head.angle.z          = deg2rad(90);
%% Upper Right limbs
RightArm.parlength   = 250;
RightArm.chilength   = 250;
RightArm.parangle.x  = deg2rad(50);
RightArm.parangle.y  = deg2rad(90);
RightArm.parangle.z  = deg2rad(160);
RightArm.chiangle.x  = deg2rad(-60);
RightArm.chiangle.y  = deg2rad(160);
RightArm.chiangle.z  = deg2rad(60);
%% Upper Left Limbs
LeftArm.parlength   = 250;
LeftArm.chilength   = 250;
LeftArm.parangle.x  = deg2rad(0);
LeftArm.parangle.y  = deg2rad(90);
LeftArm.parangle.z  = deg2rad(100);
LeftArm.chiangle.x  = deg2rad(0);
LeftArm.chiangle.y  = deg2rad(100);
LeftArm.chiangle.z  = deg2rad(60);
%% Lower Right Limbs
RightLeg.parlength   = 400;
RightLeg.chilength   = 400;
RightLeg.parangle.x  = deg2rad(0);
RightLeg.parangle.y  = deg2rad(90);
RightLeg.parangle.z  = deg2rad(10);
RightLeg.chiangle.x  = deg2rad(0);
RightLeg.chiangle.y  = deg2rad(60);
RightLeg.chiangle.z  = deg2rad(-60);
%% Lower Left Limbs
LeftLeg.parlength   = 400;
LeftLeg.chilength   = 400;
LeftLeg.parangle.x  = deg2rad(50);
LeftLeg.parangle.y  = deg2rad(90);
LeftLeg.parangle.z  = deg2rad(10);
LeftLeg.chiangle.x  = deg2rad(0);
LeftLeg.chiangle.y  = deg2rad(10);
LeftLeg.chiangle.z  = deg2rad(-20);