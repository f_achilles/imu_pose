close all
parangle = 90; 
parlength = 200 ; 
chilength = 250 ;
p1 = deg2rad(-50);
p2 = deg2rad(130);
p3 = deg2rad(-10);
c1 = deg2rad(50);
c2 = deg2rad(30);
c3 = deg2rad(-90);

%%root limb to joint point .---o(parent)
% Rot_y           = [0;1;0];   %vector along y-axis
origin          = [0;0;0];
point_rot       = [1;0;0]; %point to be rotated
Rot_Matrix_x     = [0 0 1;0 cos(p1) -sin(p1) ; 0 sin(p1) cos(p1)];
Rot_Matrix_y     = [cos(p2) 0 -sin(p2) ;0 1 0; sin(p2) 0 cos(p2)];
Rot_Matrix_z     = [cos(p3) -sin(p3) 0 ; sin(p3) cos(p3) 0 ; 0 0 1];
Rot_xyzaxes      = Rot_Matrix_z * Rot_Matrix_x * Rot_Matrix_y;     %%rotation around z axis
par_Q            = qGetQ( Rot_xyzaxes ); %child quaternion
%rotQ            = qGetRotQuaternion(par_angle,Rot_y); %Rotation Quaternion
par_rotpoint    = qRotatePoint(point_rot,par_Q); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * parlength;                      
par_end_pos     = par_rotpt_len + origin; %end point of the parent


%%joint to end effector position o---*(child)              
time             = 5;
frequency        = 0.1;
%theta1           = (sin(2 * pi * frequency * time)) + deg2rad(chiangle) ; %sine motion
%chi_Q            = qGetRotQuaternion(-theta1,Rot_y);
Rot_Matrix_x     = [0 0 1;0 cos(c1) -sin(c1) ; 0 sin(c1) cos(c1)];
Rot_Matrix_y     = [cos(c2) 0 -sin(c2) ;0 1 0; sin(c2) 0 cos(c2)];
Rot_Matrix_z     = [cos(c3) -sin(c3) 0 ; sin(c3) cos(c3) 0 ; 0 0 1];
Rot_xyzaxes      = Rot_Matrix_z * Rot_Matrix_x * Rot_Matrix_y;     %%rotation around z axis
chi_Q            = qGetQ( Rot_xyzaxes ); %child quaternion
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * chilength;
end_eff_pos      = chi_rotpt_len + par_end_pos; %end effector position 
   
   %%data for plotting
   %data for parent
   axis([-600 600,-600 600,-600 600]);
   x_par=[origin(1),par_end_pos(1)];
   y_par=[origin(2),par_end_pos(2)];
   z_par=[origin(3),par_end_pos(3)];
   %data for child
   x_chi=[par_end_pos(1),end_eff_pos(1)];
   y_chi=[par_end_pos(2),end_eff_pos(2)];
   z_chi=[par_end_pos(3),end_eff_pos(3)];
   %%plotting the data
   par_Limb = line('XData',x_par, 'YData',y_par, 'ZData',z_par,'Color','b','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
   chi_Limb = line('XData',x_chi, 'YData',y_chi, 'ZData',z_chi,'Color','g','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
   axis vis3d tight