%function StickFig_plot ()
%%%just for a ninety degree rotation i used the Quaternions for a perfect
%%%rotation
body1_length    = 200;%middle line
body2_length    = -50;%for the bottom 
origin          = [0;0;0];
rot_y           = [0,1,0];
point_rot       = [1;0;0]; 
body_angle      = deg2rad(-90);   
rotQ            = qGetRotQuaternion(body_angle,rot_y); 
par_rotpoint    = qRotatePoint(point_rot,rotQ); 
par1_rotpt_len  = par_rotpoint * body1_length;                      
body1           = par1_rotpt_len + origin; 
par2_rotpt_len  = par_rotpoint * body2_length;                      
body2           = par2_rotpt_len + origin;

%% upper body hypotenuse(support-shoulders)
shoulder    = [200;0;0];
hypot1      = sqrt((body1.^2) + (shoulder.^2));
hypot2      = [-hypot1(1);hypot1(2);hypot1(3)];

%% lower body hypotenuse(support - abdomen)
abd     = [100;0;0];
hypot3  = -sqrt((body2.^2) + (abd.^2));
hypot4  = [-hypot3(1);hypot3(2);hypot3(3)];
%% plotting the body without arms and legs
xx  =[origin(1),body1(1)];
yy  =[origin(2),body1(2)];
zz  =[origin(3),body1(3)];
xx1 =[origin(1),hypot1(1)];
yy1 =[origin(2),hypot1(2)];
zz1 =[origin(3),hypot1(3)];
xx2 =[origin(1),hypot2(1)];
yy2 =[origin(2),hypot2(2)];
zz2 =[origin(3),hypot2(3)];
xx3 =[origin(1),hypot3(1)];
yy3 =[origin(2),hypot3(2)];
zz3 =[origin(3),hypot3(3)];
xx4 =[origin(1),hypot4(1)];
yy4 =[origin(2),hypot4(2)];
zz4 =[origin(3),hypot4(3)];
axis([-800 800,-800 800,-800 800]);
hold on
body_main   = line('XData',xx, 'YData',yy, 'ZData',zz,'Color','r','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
body_ur     = line('XData',xx1, 'YData',yy1, 'ZData',zz1,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
body_ul     = line('XData',xx2, 'YData',yy2, 'ZData',zz2,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
body_lr     = line('XData',xx3, 'YData',yy3, 'ZData',zz3,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
body_ll     = line('XData',xx4, 'YData',yy4, 'ZData',zz4,'Color','y','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
hold off
 
%% upper right arm with parent and child
%parent
par_angle       = deg2rad(-80);   %parent angle
rotQ            = qGetRotQuaternion(par_angle,rot_y); %Rotation Quaternion
par_rotpoint    = qRotatePoint(point_rot,rotQ); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * 300; 
parent          = par_rotpt_len + hypot1;
%end point of the parent

%child    
time             = 5;
frequency        = 0.1;
theta1           = (sin(2 * pi * frequency * time)) + deg2rad(20) ; %sine motion
chi_Q            = qGetRotQuaternion(-theta1,rot_y);
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * 350;
child      = chi_rotpt_len + parent;
disp(parent);
disp(child);
urx=[hypot1(1),parent(1)];
ury=[hypot1(2),parent(2)];
urz=[hypot1(3),parent(3)];
urx1=[parent(1),child(1)];
ury1=[parent(2),child(2)];
urz1=[parent(3),child(3)];

hold on
par_Limb = line('XData',urx, 'YData',ury, 'ZData',urz,'Color','b','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
chi_Limb = line('XData',urx1, 'YData',ury1, 'ZData',urz1,'Color','g','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
hold off


%%initializing arms and legs along with plotting.
%%upper left arm with parent and child
%parent
par_angle       = deg2rad(-30);   %parent angle
rotQ            = qGetRotQuaternion(par_angle,rot_y); %Rotation Quaternion
par_rotpoint    = qRotatePoint(point_rot,rotQ); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * 300; 
parent          = par_rotpt_len + hypot2;
%end point of the parent

%child    
time             = 5;
frequency        = 0.1;
theta1           = (sin(2 * pi * frequency * time)) + deg2rad(90) ; %sine motion
chi_Q            = qGetRotQuaternion(-theta1,rot_y);
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * 350;
child      = chi_rotpt_len + parent;
disp(parent);
disp(child);
ulx=[hypot2(1),parent(1)];
uly=[hypot2(2),parent(2)];
ulz=[hypot2(3),parent(3)];
ulx1=[parent(1),child(1)];
uly1=[parent(2),child(2)];
ulz1=[parent(3),child(3)];

hold on
par1_Limb = line('XData',ulx, 'YData',uly, 'ZData',ulz,'Color','b','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
chi1_Limb = line('XData',ulx1, 'YData',uly1, 'ZData',ulz1,'Color','g','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
hold off
%%
%%lower right leg with parent and child
%parent
par_angle       = deg2rad(-90);   %parent angle
rotQ            = qGetRotQuaternion(par_angle,rot_y); %Rotation Quaternion
par_rotpoint    = qRotatePoint(point_rot,rotQ); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * 400; 
parent          = par_rotpt_len + hypot3;
%end point of the parent

%child    
time             = 5;
frequency        = 0.1;
theta1           = (sin(2 * pi * frequency * time)) + deg2rad(90) ; %sine motion
chi_Q            = qGetRotQuaternion(-theta1,rot_y);
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * 350;
child            = chi_rotpt_len +(parent);
disp(parent);
disp(child);
lrx =[hypot3(1),parent(1)];
lry =[hypot3(2),parent(2)];
lrz =[hypot3(3),-parent(3)];
lrx1=[parent(1),child(1)];
lry1=[parent(2),child(2)];
lrz1=[-parent(3),-child(3)];

hold on
par2_Limb = line('XData',lrx, 'YData',lry, 'ZData',lrz,'Color','b','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
chi2_Limb = line('XData',lrx1, 'YData',lry1, 'ZData',lrz1,'Color','g','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
hold off
%%
%%lower left leg with parent and child
%parent
par_angle       = deg2rad(-90);   %parent angle
rotQ            = qGetRotQuaternion(par_angle,rot_y); %Rotation Quaternion
par_rotpoint    = qRotatePoint(point_rot,rotQ); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * 400; 
parent          = par_rotpt_len + hypot4;
%end point of the parent

%child    
time             = 5;
frequency        = 0.1;
theta1           = (sin(2 * pi * frequency * time)) + deg2rad(90) ; %sine motion
chi_Q            = qGetRotQuaternion(-theta1,rot_y);
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * 350;
child      = chi_rotpt_len + parent;
disp(parent);
disp(child);
llx =[hypot4(1),parent(1)];
lly =[hypot4(2),parent(2)];
llz =[hypot4(3),-parent(3)];
llx1=[parent(1),child(1)];
lly1=[parent(2),child(2)];
llz1=[-parent(3),-child(3)];

hold on
par3_Limb = line('XData',llx, 'YData',lly, 'ZData',llz,'Color','b','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
chi3_Limb = line('XData',llx1, 'YData',lly1, 'ZData',llz1,'Color','g','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
hold off

xlabel('X Axis')
ylabel('Y Axis')
zlabel('Z Axis')
axis vis3d image



