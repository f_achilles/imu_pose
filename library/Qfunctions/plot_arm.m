%%%%%%Basically done the arm motion.I have few doubts.First check this and
%%%%%%let me know:)

%%(.---0---*)....limb
function arm_plot = plot_arm(parangle , parlength , chiangle , chilength)

%%root limb to joint point .---o(parent)
Rot_y           = [0;1;0];   %vector along y-axis
origin          = [0;0;0];
point_rot       = [1;0;0];       %point to be rotated
par_angle       = deg2rad(-parangle);   %parent angle
rotQ            = qGetRotQuaternion(par_angle,Rot_y); %Rotation Quaternion
par_rotpoint    = qRotatePoint(point_rot,rotQ); %Rotating the point according to the rotation quaternion
par_rotpt_len   = par_rotpoint * parlength;                      
par_end_pos     = par_rotpt_len + origin; %end point of the parent


%%joint to end effector position o---*(child)              
time             = 5;
frequency        = 0.1;
theta1           = (sin(2 * pi * frequency * time)) + deg2rad(chiangle) ; %sine motion
chi_Q            = qGetRotQuaternion(-theta1,Rot_y);
% Rot_Matrix_z     = [cos(theta1) -sin(theta1) 0 ; sin(theta1) cos(theta1) 0 ; 0 0 1];
% Rot_xyzaxes      = Rot_Matrix_z;     %%rotation around z axis
% chi_Q            = qGetQ( Rot_xyzaxes ); %child quaternion
chi_rotpt        = qRotatePoint(point_rot,chi_Q); %rotating the point according to the child quaternion
chi_rotpt_len    = chi_rotpt * chilength;
end_eff_pos      = chi_rotpt_len + par_end_pos; %end effector position 
   
   %%data for plotting
   %data for parent
   axis([-400 400,-400 400,-400 400]);
   x_par=[origin(1),par_end_pos(1)];
   y_par=[origin(2),par_end_pos(2)];
   z_par=[origin(3),par_end_pos(3)];
   %data for child
   x_chi=[par_end_pos(1),end_eff_pos(1)];
   y_chi=[par_end_pos(2),end_eff_pos(2)];
   z_chi=[par_end_pos(3),end_eff_pos(3)];
   %%plotting the data
   par_Limb = line('XData',x_par, 'YData',y_par, 'ZData',z_par,'Color','b','LineStyle','--','Marker','o','MarkerEdgeColor','b', 'MarkerSize',6, 'LineWidth',2);
   chi_Limb = line('XData',x_chi, 'YData',y_chi, 'ZData',z_chi,'Color','g','LineStyle','--','Marker','+', 'MarkerSize',6, 'LineWidth',2);
  
end  
   
  
   
   


