function [sensorNamesCellArray, signalNamesCellArray, formatNamesCellArray, signalUnitsCellArray, sensorDataArray] = loadshimd_f(filePath)

%LOADSHIMD - Reads Shimmer data from a file which uses SHIMDv1 or
%              SHIMDv2 formats.
%
%  LOADSHIMD(FILEPATH) loads the header and numeric data from the file 
%  defined in FILEPATH. 
%
%  SYNOPSIS: [sensorNamesCellArray, signalNamesCellArray, signalUnitsCellArray, sensorDataArray] = loadshimd(filePath)
%
%  INPUT : filePath - String value defining the name/path of the file that 
%                     data is loaded from.
% 
%  OUTPUT: sensorNamesCellArray - [n x 1] cell array containing the sensor
%                                 names where n = number of data signals.
%  OUTPUT: signalNamesCellArray - [n x 1] cell array containing the signal 
%                                 names where n = number of data signals.
%  OUTPUT: formatNamesCellArray - [n x 1] cell array containing the signal 
%                                 formats where n = number of data signals.
%  OUTPUT: signalUnitsCellArray - [n x 1] cell array containing the signal 
%                                 units where n = number of data signals.
%  OUTPUT: sensorDataArray - [m x n] numeric array containing the sensor 
%                            data where m = number of samples and n = number
%                            of data signals.
%
%  EXAMPLE: [sensorNamesCellArray, signalNamesCellArray, signalUnitsCellArray, sensorDataArray] = loadshimd('shimmerdata.dat')
%
%  Created: 3rd January 2013, Karol O'Donovan <support@shimmer-research.com>
%

%% File format is always SHIMDv2
nHeaderRows = 4;

%% Load numeric data
% Use this for the calibrated timestamps: datenum('08/02/2015 17:41:18.006','dd/mm/yyyy HH:MM:SS.FFF')
sensorDataArray = dlmread (filePath,'\t',nHeaderRows,0);	               % Load tab seperated numeric data from a text file starting 
                                                                           % from 4th/5th row 1st column of data(i=0,1,2...) (i.e. ignore header information)                                                                                                   

%% Load header data

[nSamples, nSignals] = size(sensorDataArray);                              % Determine number of signals captured in the file (i.e. columns of data)     

fid = fopen(filePath);

sensorNamesCellTemp = textscan(fid, '%s', nSignals, 'delimiter' , '\t');   % Load tab seperated sensor name strings for each signal  
sensorNamesCellArray = sensorNamesCellTemp{1}';
signalNamesCellTemp = textscan(fid, '%s', nSignals, 'delimiter' , '\t');   % Load tab seperated signal name strings for each signal  
signalNamesCellArray = signalNamesCellTemp{1}';

% SHIMDv2 file format
formatNamesCellTemp = textscan(fid, '%s', nSignals, 'delimiter' , '\t');   % Load tab seperated data format name strings for each signal  
formatNamesCellArray = formatNamesCellTemp{1}';  

signalUnitsCellTemp = textscan(fid, '%s', nSignals, 'delimiter' , '\t');   % Load tab seperated signal unit strings for each signal  
signalUnitsCellArray = signalUnitsCellTemp{1}';


fclose(fid);

end