function radianAngles = deg2rad(degreeAngles)

radianAngles = pi*degreeAngles/180;

end